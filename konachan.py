# Improvise a bit more for random images

# Fix tags bc of URI encoding

import requests
import os
import urllib.parse


limit = int(input("Enter the limit of images: "))

tagsInp = input("Enter comma sperated tags: ").split(",")

tags = ''

for i in range(0,len(tagsInp)):
    tags = tagsInp[i] + '+' + tags

tags = tags + 'order:random' +'+-loli' + '+-guro' #Enter blacklisted tags here with '+-' in front
#print(tags)

payload = {
    "limit":limit,
    "tags":tags}

# ref - https://stackoverflow.com/questions/23496750/how-to-prevent-python-requests-from-percent-encoding-my-urls/23497912
payload_str = "&".join("%s=%s" % (k,v) for k,v in payload.items())
# Fixed unwanted URI encoding for tags

url = "https://konachan.net/post.json"

# gets images from api
r = requests.get(url,params=payload_str)

#print(r.url)

# converts into readable json
jsonData = r.json()

#print(r.json())

# Create Directory
Dir = "."+os.sep+"konachan_scraped"+os.sep
if not os.path.isdir(Dir):
    os.mkdir(Dir)

# Downloads images
i = 0
count = 0
while count < limit:
    try:
        extension = '.'+jsonData[i]['file_url'].split(".")[-1]
        name = str(jsonData[i]['id'])+extension
    except:
        i+=1
        continue
    if os.path.isfile(Dir+name):
        i-=1
    else:
        try:
            print("Downloading image: ",jsonData[i]['file_url'])
            with open(Dir+name,"wb") as img:
                down = requests.get(jsonData[i]['file_url'])
                img.write(down.content)
        except KeyboardInterrupt:
            print("\n^C detected, Exiting...")
            exit()
        except:
            print("Error faced while downloading Image")
        i+=1
        count+=1



# This is using Beautiful soup instead of JSON

# Use json over this as this requires scrapping the first page and then the image page

# Json gives all the image data at once 

# API > Scraping 

import requests
from bs4 import BeautifulSoup as BS
import os

url = "https://safebooru.org/index.php?page=post&s=list"

payload = "&tags="

inp = input("Enter tags comma sperated: ").split(",")
tags = ''
for x in inp:
    tags = x + '+' + tags

payload=payload+tags

r = requests.get(url+payload)

soup = BS(r.text, 'lxml')

links = soup.find_all('span',class_='thumb')
print(links[0].prettify())
exit()
for i in range(len(links)):
    links[i]['id'] = str(links[i]['id'])[1:]
    #print("Image link: ",end="")
    print(f"https://safebooru.org/index.php?page=post&s=view&id={links[i]['id']}")



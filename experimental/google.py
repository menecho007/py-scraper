# Make it more specific and better?


import requests 
from bs4 import BeautifulSoup

url = "https://google.com/search"

inp = input("Search term: ").replace(' ',"+")

payload = {"q":inp}

r = requests.get(url,params=payload)

soup = BeautifulSoup(r.text,'lxml')

divmain = soup.find('div',id='main')

lnks = divmain.findAll('div',class_='ZINbbc xpd O9g5cc uUPGi')

for i in range(len(lnks)):
    try:
        print('info: ',lnks[i].h3.text)
        print('link: ','https://google.com'+lnks[i].a['href'])
        print("Description: ",lnks[i].text)
    except:
        pass
    print()

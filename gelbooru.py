import requests
import os
import urllib.parse


limit = int(input("Enter the limit of images: "))

tagsInp = input("Enter comma sperated tags: ").split(",")

tags = ''

for i in range(0,len(tagsInp)):
    tags = tagsInp[i] + '+' + tags

tags = tags + 'sort:random' +'+-loli' + '+-guro' #Enter blacklisted tags here with '+-' in front

#print(tags)

payload = {
    "api_key":"5cc45a6baae91f7fa4707a20f74df4d73645789a0b6f99d06632ad0afb822de2",
    "user_id":"594062",
    "page":"dapi",
    "s":"post",
    "q":"index",
    "limit":limit * 10,
    "tags":tags,
    "json":1 
    }

# ref - https://stackoverflow.com/questions/23496750/how-to-prevent-python-requests-from-percent-encoding-my-urls/23497912
payload_str = "&".join("%s=%s" % (k,v) for k,v in payload.items())
# Fixed unwanted URI encoding for tags

url = "https://gelbooru.com/index.php"

# gets images from api
r = requests.get(url,params=payload_str)

#print(r.url)

# converts into readable json
jsonData = r.json()

#print(r.json())

# Create Directory
Dir = "."+os.sep+"gelbooru_scraper"+os.sep
if not os.path.isdir(Dir):
    os.mkdir(Dir)


# Downloads images
i = 0
count = 0
while count < limit:
    name = jsonData[i]['image']
    if os.path.isfile(Dir+name):
        i-=1
    else:
        try:
            print("Downloading image: ",jsonData[i]['file_url'])
            with open(Dir+name,"wb") as img:
                down = requests.get(jsonData[i]['file_url'])
                img.write(down.content)
        except:
            print("Error faced while downloading Image")
        i+=1
        count+=1


